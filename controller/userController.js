const { User } = require("../models");
const passport = require("../lib/passport");

// function format(user) {
//   const { id, username } = user;
//   return {
//     id,
//     username,
//     accessToken: user.generateToken(),
//   };
// }

module.exports = {
  getRegister: (req, res) => {
    const title = "Register";
    res.render("register", { title });
  },

  register: (req, res, next) => {
    User.register(req.body)
      .then(() => {
        res.redirect("/login");
      })
      .catch((err) => next(err));
  },

  getLogin: (req, res) => {
    const title = "Login";
    res.render("login", { title });
  },

  // login: (req, res) => {
  //   User.authenticate(req.body).then((user) => {
  // res.redirect("/users/register");
  //     res.json(format(user));
  //   });
  // },
  login: passport.authenticate("local", {
    successRedirect: "/",
    failureRedirect: "/login",
    failureFlash: true,
  }),

  whoami: (req, res) => {
    const title = "Profile";
    res.render("profile", req.user.dataValues, { title });
    // const currentUser = req.user;
    // res.json(currentUser);
  },
  index: (req, res) => {
    const title = "Home";
    res.render("home", { title });
  },
};
