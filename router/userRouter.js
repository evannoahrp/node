const router = require("express").Router();

// Middlewares
const restrict = require("../middlewares/restrict");

// Controllers
const userController = require("../controller/userController");

// Register Page
router.get("/register", userController.getRegister);
router.post("/register", userController.register);

// Login Page
router.get("/login", userController.getLogin);
router.post("/login", userController.login);

// Profile Page
router.get("/", restrict, userController.index);

module.exports = router;
