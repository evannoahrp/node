const express = require("express");
const app = express();
const session = require("express-session");
const flash = require("express-flash");
const PORT = process.env.PORT || 8000;

app.use(express.urlencoded({ extended: false }));

app.use(
  session({
    secret: "Secret",
    resave: false,
    saveUninitialized: false,
  })
);

const passport = require("./lib/passport");
app.use(passport.initialize());
app.use(passport.session());

app.use(flash());

app.set("view engine", "ejs");

const router = require("./router");
app.use(router);
app.listen(PORT, () => {
  console.log(`App listening at http://localhost:${PORT}`);
});

// Token
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJldmFubm9haCIsImlhdCI6MTYyNjQ0MTMyMX0.eG8bo_xpW4RpdSWxdaApURHungxdM5QNygrpRbVFgP0
