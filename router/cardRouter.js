const router = require("express").Router();
const cardController = require("../controller/cardController");

router.get("/", cardController.index);
router.get("/:id", cardController.show);
router.get("/create", cardController.getCreate);
router.post("/create", cardController.create);
router.patch("/:id", cardController.update);
router.delete("/:id", cardController.delete);

module.exports = router;
