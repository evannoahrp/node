const { Article } = require("../models");

module.exports = {
  create: (req, res) => {
    const { title, body, approved } = req.body;
    Article.create({
      title,
      body,
      approved,
    })
      .then((article) => {
        res.json({
          status: 200,
          message: "Post berhasil",
          data: article,
        });
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: "Kesalahan server",
        });
      });
  },

  createArticle: (req, res) => {
    res.render("articles/create");
  },

  index: (req, res) => {
    Article.findAll({})
      .then((article) => {
        if (article !== 0) {
          res.json({
            status: 200,
            message: "Berhasil",
            data: article,
          });
        } else {
          res.json({
            status: 400,
            message: "Data tidak ditemukan",
          });
        }
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: "Kesalahan server",
        });
      });
  },

  show: (req, res) => {
    const articleId = req.params.id;
    Article.findOne({
      where: {
        id: articleId,
      },
    })
      .then((article) => {
        res.json({
          status: 200,
          message: "Berhasil",
          data: article,
        });
      })
      .catch((err) => {
        res.json({
          status: 500,
          message: "Kesalahan server",
        });
      });
  },

  update: (req, res) => {
    const articleId = req.params.id;
    const { title, body, approved } = req.body;
    Article.update(
      {
        title,
        body,
        approved,
      },
      {
        where: { id: articleId },
      }
    )
      .then((article) => {
        res.json({
          status: 201,
          data: article,
        });
      })
      .catch((err) => {
        res.json({
          status: 422,
          message: "Gak bisa update",
        });
      });
  },

  delete: (req, res) => {
    const articleId = req.params.id;
    Article.destroy({
      where: {
        id: articleId,
      },
    }).then(() => {
      res.json({
        status: 200,
        message: "Delete berhasil",
      });
      res.render("../articles/create");
    });
  },
};
